use anyhow::{Context, Result};
use pebble_bin::PebbleBinary;
use std::env::args_os;
use std::fs::{self, File};

fn main() -> Result<()> {
    let mut args = args_os();
    let in_path = args.nth(1).context("Missing input!")?;
    let out_path = args.next().context("Missing input!")?;

    let data = fs::read(in_path)?;

    let binary = PebbleBinary::new(&data)?;

    binary.write(&mut File::create(out_path)?)?;

    Ok(())
}
