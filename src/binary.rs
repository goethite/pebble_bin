use crate::stm32_crc;
use anyhow::{ensure, Context, Result};
use binrw::{BinRead, BinWrite};
use object::elf::FileHeader32;
use object::endian::LittleEndian;
use object::read::elf::ElfFile;
use object::read::{Object, ObjectSection, ObjectSegment, ObjectSymbol};
use object::RelocationKind;
use std::convert::TryInto;
use std::io::{Cursor, Write};
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Debug, PartialEq, Eq, BinRead, BinWrite)]
#[br(little)]
pub struct PebbleHeader {
    pub header_addr: u64,
    pub struct_version: u16,
    pub sdk_version: u16,
    pub app_version: u16,
    pub load_size: u16,
    pub offset: u32,
    pub crc: u32,
    pub name: [u8; 32],
    pub company: [u8; 32],
    pub icon_res_id: u32,
    pub jump_table: u32,
    pub flags: u32,
    pub num_reloc_entries: u32,
    pub uuid: [u8; 16],
    pub resource_crc: u32,
    pub resource_timestamp: u32,
    pub virtual_size: u16,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PebbleBinary<T> {
    object: T,
}

impl<'data> PebbleBinary<ElfFile<'data, FileHeader32<LittleEndian>, &'data [u8]>> {
    pub fn new(elf: &'data [u8]) -> Result<Self> {
        let object = ElfFile::parse(elf)?;
        Ok(Self { object })
    }
}

impl<'file, T> PebbleBinary<T>
where
    T: Object<'file, 'file> + 'file,
{
    pub fn raw_binary(&'file self) -> Result<Vec<u8>> {
        let base_address = match self.object.segments().map(|x| x.address()).min() {
            Some(min) => min,
            None => return Ok(Vec::new()),
        };

        let end_address = self
            .object
            .segments()
            .map(|x| x.address() + x.file_range().1)
            .max()
            .unwrap();

        let binary_length = (end_address - base_address).try_into()?;

        let mut bin = vec![0; binary_length];

        for segment in self.object.segments() {
            let pos = (segment.address() - base_address).try_into()?;

            let data = segment.data()?;

            bin[pos..][..data.len()].copy_from_slice(data);
        }

        Ok(bin)
    }

    pub fn relocations(&'file self) -> impl Iterator<Item = Result<u64>> + 'file {
        let relocations = self
            .object
            .section_by_name(".data")
            .into_iter()
            .flat_map(|x| x.relocations())
            .map(|(addr, reloc)| {
                ensure!(
                    reloc.kind() == RelocationKind::Absolute,
                    "only absolute relocations are supported"
                );
                ensure!(reloc.size() == 32, "only 32-bit relocations are supported");
                ensure!(
                    reloc.has_implicit_addend(),
                    "only implicit addends are supported"
                );

                Ok(addr)
            });

        let got_addresses = self
            .object
            .section_by_name(".got")
            .into_iter()
            .flat_map(|section| (section.address()..section.address() + section.size()).step_by(4))
            .map(Ok);

        relocations.chain(got_addresses)
    }

    pub fn get_symbol_addr(&'file self, name: &str) -> Result<u32> {
        Ok(self
            .object
            .symbols()
            .find(|x| x.name() == Ok(name))
            .map(|x| x.address())
            .context("missing symbol")?
            .try_into()?)
    }

    pub fn virtual_size(&'file self) -> Result<u64> {
        let mut end_addr = 0;

        if let Some(bss) = self.object.section_by_name(".bss") {
            end_addr = bss.address() + bss.size();
        }

        if end_addr == 0 {
            if let Some(data) = self.object.section_by_name(".data") {
                end_addr = data.address() + data.size();
            }
        }

        Ok(end_addr)
    }

    pub fn write<W: Write>(&'file self, writer: &mut W) -> Result<()> {
        let mut raw_binary = self.raw_binary()?;
        let (header_data, code) = raw_binary.split_at_mut(0x82);

        let relocations: Vec<_> = self.relocations().collect::<Result<_>>()?;

        let mut header = PebbleHeader::read(&mut Cursor::new(&header_data))?;

        header.load_size = (code.len() + 0x82).try_into()?;
        header.offset = self.get_symbol_addr("main")?;
        header.crc = stm32_crc::crc32(code);

        header.resource_crc = 0xFFFFFFFF;
        header.resource_timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)?
            .as_secs()
            .try_into()?;

        header.jump_table = self.get_symbol_addr("pbl_table_addr")?;

        // TODO: handle flags

        header.num_reloc_entries = relocations.len().try_into()?;

        header.virtual_size = self.virtual_size()?.try_into()?;

        let mut new_header_data = vec![];
        header.write_to(&mut Cursor::new(&mut new_header_data))?;

        writer.write_all(&new_header_data)?;
        writer.write_all(code)?;

        for relocation in relocations {
            let relocation: u32 = relocation.try_into()?;
            let bytes = relocation.to_le_bytes();
            writer.write_all(&bytes)?;
        }

        Ok(())
    }
}
