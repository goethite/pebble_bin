use binrw::{binread, binrw, BinResult, BinWrite, WriteOptions};
use std::io::{Seek, SeekFrom, Write};

#[binread]
#[derive(Debug, PartialEq)]
#[brw(little, assert(file_id != 0, "wrong number of files"))]
pub struct TableEntry {
    pub file_id: u32,

    #[br(temp, map = |x: u32| x + 0x100c)]
    offset: u32,

    pub length: u32,
    pub crc: u32,

    #[br(restore_position, seek_before = SeekFrom::Start(offset as u64), count = length)]
    pub contents: Vec<u8>,
}

#[allow(clippy::ptr_arg)]
fn write_entries<W: Write + Seek>(
    this: &Vec<TableEntry>,
    writer: &mut W,
    opts: &WriteOptions,
    _: (),
) -> BinResult<()> {
    let mut offset = 0;

    for entry in this {
        entry.file_id.write_options(writer, opts, ())?;
        offset.write_options(writer, opts, ())?;
        entry.length.write_options(writer, opts, ())?;
        entry.crc.write_options(writer, opts, ())?;

        offset += entry.length;
    }

    let pos = writer.stream_position()?;

    let pad_len = if this.is_empty() { 0xffc } else { 0x100c };

    for _ in pos..pad_len {
        writer.write_all(&[0])?;
    }

    for entry in this {
        writer.write_all(&entry.contents)?;
    }

    Ok(())
}

#[binrw]
#[derive(Debug, PartialEq)]
#[brw(little)]
pub struct ResourcePack {
    pub num_files: u32,
    pub crc: u32,
    pub timestamp: u32,

    #[br(count = num_files)]
    #[bw(write_with = write_entries)]
    pub table_entries: Vec<TableEntry>,
}
