use anyhow::{Context, Result};
use binrw::{BinRead, BinWrite};
use pebble_bin::pbpack::ResourcePack;
use std::env::args_os;
use std::fs::File;

fn main() -> Result<()> {
    let mut args = args_os();
    let in_path = args.nth(1).context("Missing input!")?;
    let out_path = args.next().context("Missing output!")?;

    let mut in_file = File::open(in_path)?;
    let mut out_file = File::create(out_path)?;

    let pbpack = ResourcePack::read(&mut in_file)?;
    pbpack.write_to(&mut out_file)?;

    Ok(())
}
