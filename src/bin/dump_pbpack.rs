use anyhow::{Context, Result};
use binrw::BinRead;
use pebble_bin::pbpack::ResourcePack;
use std::env::args_os;
use std::fs::File;

fn main() -> Result<()> {
    let mut args = args_os();
    let in_path = args.nth(1).context("Missing input!")?;

    let mut file = File::open(in_path)?;

    let pbpack = ResourcePack::read(&mut file)?;

    println!("{:#x?}", pbpack);

    Ok(())
}
