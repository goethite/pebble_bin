use anyhow::{Context, Result};
use pebble_bin::stm32_crc;
use std::env::args_os;
use std::fs;

fn main() -> Result<()> {
    let mut args = args_os();
    let in_path = args.nth(1).context("Missing input!")?;

    let data = fs::read(in_path)?;

    let crc = stm32_crc::crc32(&data);

    println!("{}", crc);

    Ok(())
}
