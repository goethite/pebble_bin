use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct WatchappInfo {
    pub watchface: bool,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Resources {
    pub media: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AppInfo {
    pub target_platforms: Vec<String>,
    pub display_name: String,
    pub name: String,
    pub message_keys: HashMap<String, usize>,
    pub company_name: String,

    #[serde(rename = "enableMultiJS")]
    pub enable_multi_js: bool,

    pub version_label: String,
    pub capabilities: Vec<String>,
    pub sdk_version: String,
    pub app_keys: HashMap<String, usize>,
    pub long_name: String,
    pub short_name: String,
    pub watchapp: WatchappInfo,
    pub resources: Resources,
    pub uuid: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Version {
    pub major: usize,
    pub minor: usize,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FileInfo {
    pub timestamp: usize,
    pub crc: u32,
    pub name: String,
    pub size: usize,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BinaryInfo {
    pub sdk_version: Version,

    #[serde(flatten)]
    pub file: FileInfo,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct BinaryManifest {
    pub manifest_version: usize,
    pub generated_by: String,
    pub generated_at: usize,
    pub application: BinaryInfo,
    pub debug: HashMap<String, String>,
    pub r#type: String,
    pub resources: FileInfo,
}
