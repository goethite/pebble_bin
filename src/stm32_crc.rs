use crc::*;

pub fn crc32(data: &[u8]) -> u32 {
    let crc = Crc::<u32>::new(&CRC_32_MPEG_2);
    let mut digest = crc.digest();

    for word in data.chunks(4) {
        if word.len() == 4 {
            for &byte in word.iter().rev() {
                digest.update(&[byte]);
            }
        } else {
            for _ in word.len()..4 {
                digest.update(&[0]);
            }

            digest.update(word);
        }
    }

    digest.finalize()
}

#[cfg(test)]
mod test {
    #[test]
    fn crc32_exact() {
        assert_eq!(super::crc32(b"\xfe\xff\xfe\xff"), 0x519b130);
    }

    #[test]
    fn crc32_padded() {
        assert_eq!(super::crc32(b"123 567 901 34"), 0x89f3bab2);
        assert_eq!(super::crc32(b"123456789"), 0xaff19057);
        assert_eq!(super::crc32(b"\xfe\xff\xfe\xff\x88"), 0x495e02ca);
    }
}
